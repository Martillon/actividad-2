using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerTankController : MonoBehaviour
{
    
    public float speed=4f;
    public float angularspeed=30f;

    // Update is called once per frame
    void Update()
    {
        //Rotación
        if(Input.GetKey(KeyCode.A)){
            transform.Rotate(Vector3.up *  (-angularspeed*Time.deltaTime));
        }
        if(Input.GetKey(KeyCode.D)){
            transform.Rotate(Vector3.up *  (angularspeed*Time.deltaTime));
        }
        //Movimiento
        if(Input.GetKey(KeyCode.W)){
            transform.Translate(Vector3.forward * (Time.deltaTime * speed));
        }
        if(Input.GetKey(KeyCode.S)){
            transform.Translate(Vector3.back * (Time.deltaTime * speed));
        }
    }
}
