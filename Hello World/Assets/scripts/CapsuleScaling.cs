using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CapsuleScaling : MonoBehaviour
{
    
    [SerializeField]
    private Vector3 axes;  //posibles ejes de escalado
    public float scaleUnits;   //velocidad de escalado

    void Update()
    {
        //acotacion del valor unitario entre [-1,1]
        axes=CapsuleMovement.ClampVector3(axes);
        //La escala es acumulativa, al contrario del movimiento o la rotacion
        //Es decir, anadimos el nuevo valor al ya existente
        transform.localScale += axes*(scaleUnits*Time.deltaTime);
    }
}
