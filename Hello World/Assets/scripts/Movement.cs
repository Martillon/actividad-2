using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{
    [SerializeField]
    private Vector3 direction;   //Posibles ejes de movimiento
    public float speed;          //velocidad de movimiento

    void Update()
    {
        direction=ClampVector3(direction);  //Acotacion de valores de direccion a valores unitarion [-1,1]

        transform.Rotate(direction * (speed * Time.deltaTime)); //Transforma desplazamiento en base del tiempo
    }
    /*Permite acotar los valores del componente vector 3 entre 1 y -1*/
    public static Vector3 ClampVector3(Vector3 target){

        float clampedX=Mathf.Clamp(target.x,-1f,-1f);
        float clampedY=Mathf.Clamp(target.y,-1f,-1f);
        float clampedZ=Mathf.Clamp(target.z,-1f,-1f);

        Vector3 result= new Vector3(clampedX,clampedY,clampedZ);

        return result;
    }
}
